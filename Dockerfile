FROM nginx:latest

COPY demo/www /usr/share/nginx/html
COPY demo/nginx.conf /etc/nginx/nginx.conf